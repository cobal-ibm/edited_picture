       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Ex1.
       
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 

       01  EDT-Pic1              PIC **999.
       01  EDT-Pic2              PIC $(6).
       01  EDT-Pic3              PIC $**9.00.
       01  EDT-Pic4              PIC +++++.

       PROCEDURE DIVISION.
      ********************************************* 
      *    MOVE 12345 TO EDT-Ex1
      *    DISPLAY "Ex1 Picture1 Data1 : " EDT-Ex1  
      *    MOVE 01234 TO EDT-Ex1  
      *    DISPLAY "Ex1 Picture1 Data2 : " EDT-Ex1 
      *    MOVE 00123 TO EDT-Ex1  
      *    DISPLAY "Ex1 Picture1 Data3 : " EDT-Ex1
      *    MOVE 00012 TO EDT-Ex1  
      *    DISPLAY "Ex1 Picture1 Data4 : " EDT-Ex1

      *********************************************     
      *********************************************
      *    MOVE "412345" TO EDT-Pic2   
      *    DISPLAY "Ex1 Picture1 Data1 : " EDT-Pic2
      *    MOVE "000123" TO EDT-Pic2  
      *    DISPLAY "Ex1 Picture2 Data2 : " EDT-Pic2
      *    MOVE "000001" TO EDT-Pic2  
      *    DISPLAY "Ex1 Picture2 Data3 : " EDT-Pic2  
      *    MOVE "000000" TO EDT-Pic2
      *    DISPLAY "Ex1 Picture2 Data4 : " EDT-Pic2 

      *********************************************

      *********************************************
      *    MOVE "0123.45" TO EDT-Pic3 
      *    DISPLAY "Ex1 Picture2 Data1 : " EDT-Pic3
      *    MOVE "0001.23" TO EDT-Pic3 
      *    DISPLAY "Ex1 Picture2 Data2 : " EDT-Pic3 
      *    MOVE "0000.25" TO EDT-Pic3 
      *    DISPLAY "Ex1 Picture2 Data3 : " EDT-Pic3 
      *    MOVE "0000.00" TO EDT-Pic3 
      *    DISPLAY "Ex1 Picture2 Data4 : " EDT-Pic3

      *********************************************  
           
           MOVE "1234" TO EDT-Pic4 
           DISPLAY "Ex1 Picture4 Data1 : " EDT-Pic4
           MOVE "-0012" TO EDT-Pic4 
           DISPLAY "Ex1 Picture4 Data2 : " EDT-Pic4  
           MOVE "0004" TO EDT-Pic4 
           DISPLAY "Ex1 Picture4 Data3 : " EDT-Pic4
           MOVE "0000" TO EDT-Pic4 
           DISPLAY "Ex1 Picture4 Data4 : " EDT-Pic4  

           .
       